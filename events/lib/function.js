var AWS = require('aws-sdk');
var config = require('./../config.json');
var dynamodb = new AWS.DynamoDB();

module.exports.updateProfileActivity = function(event_id, primaryKey, updateAttr,fn){
    var param = {
            TableName: config.DDB_PROFILEACTIVITY_TABLE,
            Key: {
                    id: {
                            S: primaryKey
                    },
                    eventId:{
                            S:event_id
                    }
            },
            AttributeUpdates: updateAttr
    };
    console.log(JSON.stringify(param));
    dynamodb.updateItem(param,fn);
}

module.exports.getProfileActivity = function(event_id, primaryKey,fn){
    var param = {
            TableName: config.DDB_PROFILEACTIVITY_TABLE,
            KeyConditionExpression: '#hashkey = :hk_val AND #rangekey = :rk_val',
            ExpressionAttributeNames: {
            '#hashkey': 'id',
            '#rangekey': 'eventId',
            },
            ExpressionAttributeValues: {
            ':hk_val': {
                        S: primaryKey
                    },
            ':rk_val':{
                        S:event_id
                    }
            }
    };
    dynamodb.query(param,fn);
}

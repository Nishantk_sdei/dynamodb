var common_function = require('./function');

module.exports.addevents = function(event, cb){
	if (typeof event.Records != "undefined") {
		var DynamoReq = event.Records;
		var totalcalls = DynamoReq.length;
		var maindone = 0;
		for(i=0;i<totalcalls;i++){
	    	var curRecord = DynamoReq[i];
	    	if(curRecord['eventName'] == "MODIFY"){
    			var newData = curRecord.dynamodb.NewImage;
    			var eventId = newData.id.S;
    			var universityId = newData.universityId.S;
    			var leaders = new Array();
    			var bouncers = new Array();
    			if(typeof newData.leaders != "undefined"){
					leaders = newData.leaders.L;
    			}
    			if(typeof newData.bouncers != "undefined"){
    				bouncers =  newData.bouncers.L;
    			}
    			var totalUser = leaders.length + bouncers.length;
    			var applied = 0;
				console.log(JSON.stringify(bouncers));
    			//For leader
    			for(j=0;j<leaders.length;j++){
				  var primaryKey = universityId+'#'+leaders[j].M.id.S+'#1';

				 	  common_function.getProfileActivity(eventId, primaryKey, function(errDetail, dataDetail){

					  	if(!errDetail){
	            			if(dataDetail.Count == 1){
	            				var ProfileActivityData = dataDetail.Items[0];
	            				var arrRoles = new Array();
	            				for(r=0;r<ProfileActivityData.roles.L.length;r++){
	            					var role = ProfileActivityData['roles']['L'][r]['S'];
	            					arrRoles.push(role);
	            				}
	            				if(arrRoles.indexOf('leader') == -1){
	            					arrRoles.push('leader');
	            				}
	            				var updRoles = new Array();
	            				for(nr=0;nr<arrRoles.length;nr++){
            						updRoles.push({S:arrRoles[nr]});
	            				}
	            				var updateAttr = {};
	            				updateAttr['roles'] = {
				                                  Action: 'PUT',
				                                  Value: {
				                                          L: updRoles
				                                  }
				                };
				                common_function.updateProfileActivity(eventId, primaryKey, updateAttr, function(errSave, dataSave){
	            					applied++;
	            					if(applied == totalUser){
	            						maindone++;
	            						if(maindone == totalcalls){

						    				return cb(null, {"success":"done"});
						    			}						
									}
	            					
	            				})
	            			}
	            			else{
	            				applied++;
	            				if(applied == totalUser){
									maindone++;
									if(maindone == totalcalls){
					    				return cb(null, {"success":"done"});
					    			}						
								}
	            			}
	            			
	            		}
	            		else{
	            			applied++;
	            			if(applied == totalUser){
								maindone++;
								if(maindone == totalcalls){
				    				return cb(null, {"success":"done"});
				    			}						
							}
	            		}
		            }); 
    			}
    			// end leader


    			//For leader
    			/*
    			for(j=0;j<bouncers.length;j++){
				  var primaryKey = universityId+'#'+bouncers[j].M.id.S+'#1';

				 	  common_function.getProfileActivity(eventId, primaryKey, function(errDetail, dataDetail){

					  	if(!errDetail){
	            			console.log("Count is",dataDetail.Count)
	            			if(dataDetail.Count == 1){
	            				var ProfileActivityData = dataDetail.Items[0];
	            				var arrRoles = new Array();
	            				for(r=0;r<ProfileActivityData.roles.L.length;r++){
	            					var role = ProfileActivityData['roles']['L'][r]['S'];
	            					console.log("role = "+role);
	            					arrRoles.push(role);
	            				}
	            				if(arrRoles.indexOf('bouncer') == -1){
	            					arrRoles.push('bouncer');
	            				}
	            				var updRoles = new Array();
	            				for(nr=0;nr<arrRoles.length;nr++){
            						updRoles.push({S:arrRoles[nr]});
	            				}
	            				var updateAttr = {};
	            				updateAttr['roles'] = {
				                                  Action: 'PUT',
				                                  Value: {
				                                          L: updRoles
				                                  }
				                };
				                console.log(updateAttr);
	            				common_function.updateProfileActivity(eventId, primaryKey, updateAttr, function(errSave, dataSave){
	            					console.log(errSave);
	            					console.log(dataSave);
	            					applied++;
	            					if(applied == totalUser){
	            						maindone++;
	            						console.log(maindone, totalcalls);
										if(maindone == totalcalls){

						    				return cb(null, {"success":"done"});
						    			}						
									}
	            					
	            				})
	            			}
	            			else{
	            				applied++;
	            				if(applied == totalUser){
									maindone++;
									console.log(maindone, totalcalls);
									console.log("Second one is running");
									console.log("Total call is", totalcalls, maindone);
									if(maindone == totalcalls){
					    				return cb(null, {"success":"done"});
					    			}						
								}
	            			}
	            			
	            		}
	            		else{
	            			applied++;
	            			if(applied == totalUser){
								maindone++;
								console.log(maindone, totalcalls);
								console.log("Third one is running");
								if(maindone == totalcalls){
				    				return cb(null, {"success":"done"});
				    			}						
							}
	            		}
		            }); 
    			}
    			*/
    			// end leader





    		}
    		else{
    			maindone++;
    			console.log(maindone);
    			if(maindone == totalcalls){
    				return cb(null, {"success":"done"});
    			}
    		}
	    }
	}
}
/*
module.exports.addevents_old = function(event, cb){
	if (typeof event.Records != "undefined") {
	    var DynamoReq = event.Records;
	    var totalcalls = DynamoReq.length;
	    var done = 0;
	    var newData = DynamoReq.dynamodb.NewImage;

	    var event_Id = newData.id.S;
	    var university_Id = newData.universityId.S;
	    var leaders = [];

	    var t = {"name":"hitsh jain", "test1":"1"}
	    
	    for(key in t){
    		console.log(t[key])
	    }
	    for(var i=0;)

	            var updateAttr = {};
                updateAttr['active'] = {
                                  Action: 'PUT',
                                  Value: {
                                          BOOL: true
                                  }
                };
                common_function.updateEvent(event_Id, updateAttr, function(errUpdate, dataUpdate){
                  if (errUpdate) {
                    console.log("There is some issue in update event");
                    console.log(errUpdate);
                  }
                  else{
                    console.log("Event has been updated");
                  }
                            
                });

  	}
}
*/